{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections   #-}
module KMeans.Core
    ( Point(..)
    , Cluster(..)
    , __remoteTable
    , distrKMeans
    )
    where

import           Control.Distributed.Process
import           Control.Distributed.Process.Closure
import           Data.Array                          (Array, bounds, (!))
import           Data.Function                       (on)
import           Data.List                           (minimumBy)
import           Data.Map                            (Map)
import qualified Data.Map                            as Map (elems, fromList, size, toList)
import           MapReduce.Basic

import           MapReduce.Distributed               hiding (__remoteTable)

type Point = (Double, Double)

type Cluster = Point

distanceSquare :: Point -> Point -> Double
distanceSquare (x1, y1) (x2, y2) = (x1-x2)^2 + (y1-y2)^2

avg :: Fractional a => [a] -> a
avg xs = sum xs / fromIntegral (length xs)

center :: [Point] -> Point
center ps = let (xs, ys) = unzip ps in (avg xs, avg ys)

nearest :: Point -> [Cluster] -> Cluster
nearest p = minimumBy (compare `on` distanceSquare p)

kmeans :: Array Int Point -> MapReduce (Int, Int) [Cluster] Cluster Point ([Point], Point)
kmeans points = MapReduce {
    mrMap = \(lo, hi) cs -> [ let p = points ! i in (nearest p cs, p)
                            | i <- [lo..hi]
                            ]
  , mrReduce = \_ ps -> (ps, center ps)
  }

-- Reification of input and output types
dictIn :: SerializableDict ((Int, Int), [Cluster])
dictIn = SerializableDict

dictOut :: SerializableDict [(Cluster, Point)]
dictOut = SerializableDict

dictInReduce :: SerializableDict (Map Cluster [Point])
dictInReduce = SerializableDict

dictOutReduce :: SerializableDict (Map Cluster ([Point], Point))
dictOutReduce = SerializableDict


remotable ['kmeans, 'dictIn, 'dictOut, 'dictInReduce, 'dictOutReduce]

distrKMeans :: Array Int Point
            -> [Cluster]
            -> [NodeId]
            -> Int
            -> Process (Map Cluster ([Point], Point))
distrKMeans points cs mappers iterations =
    distributedMapReduce $(mkStatic 'dictIn)
                   $(mkStatic 'dictOut)
                   $(mkStatic 'dictInReduce)
                   $(mkStatic 'dictOutReduce)
                   ($(mkClosure 'kmeans) points)
                   mappers
                   (go (iterations - 1))
  where
    go :: Int
       -> (Map (Int, Int) [Cluster] -> Process (Map Cluster ([Point], Point)))
       -> Process (Map Cluster ([Point], Point))
    go 0 iteration =
      iteration (Map.fromList $ map (, cs) segments)
    go n iteration = do
      clusters <- go (n - 1) iteration
      let centers = map snd $ Map.elems clusters
      iteration (Map.fromList $ map (, centers) segments)

    segments :: [(Int, Int)]
    segments = let (lo, _) = bounds points in dividePoints numPoints lo

    dividePoints :: Int -> Int -> [(Int, Int)]
    dividePoints pointsLeft offset
      | pointsLeft <= pointsPerMapper = [(offset, offset + pointsLeft - 1)]
      | otherwise = let offset' = offset + pointsPerMapper in
                    (offset, offset' - 1)
                  : dividePoints (pointsLeft - pointsPerMapper) offset'

    pointsPerMapper :: Int
    pointsPerMapper =
      ceiling (toRational numPoints / toRational (length mappers))

    numPoints :: Int
    numPoints = let (lo, hi) = bounds points in hi - lo + 1
