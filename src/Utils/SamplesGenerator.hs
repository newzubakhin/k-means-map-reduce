module Utils.SamplesGenerator
    ( generate
    )
where
import           Control.Monad
import           Data.List
import           Data.Random.Normal
import           KMeans.Core        (Point)
import           System.Random

minX, maxX, minY, maxY, minSD, maxSD :: Double
minX = -10
maxX = 10
minY = -10
maxY = 10
minSD = 1.5
maxSD = 2.0

generate :: Int -> Int -> Int -> Int -> IO ()
generate n minp maxp seed = do
    setStdGen (mkStdGen seed)

    nps <- replicateM n (randomRIO (minp, maxp))
    xs  <- replicateM n (randomRIO (minX, maxX))
    ys  <- replicateM n (randomRIO (minY, maxY))
    sds <- replicateM n (randomRIO (minSD, maxSD))

    let params = zip5 nps xs ys sds sds

    -- first generate a set of points for each set of sample parameters
    ss <- mapM (\(a,b,c,d,e) -> generate2DSamples a b c d e) params
    let points = concat ss

    -- dump all the points into stdin
    mapM_ printPoint points

printPoint :: Point -> IO ()
printPoint (x, y) = do
  putStr (show x)
  putChar ' '
  putStr (show y)
  putChar '\n'

generate2DSamples :: Int                 -- number of samples to generate
                  -> Double -> Double    -- X and Y of the mean
                  -> Double -> Double    -- X and Y standard deviations
                  -> IO [Point]
generate2DSamples n mx my sdx sdy = do
  gen <- newStdGen
  let (genx, geny) = split gen
      xsamples = normals' (mx,sdx) genx
      ysamples = normals' (my,sdy) geny
  return (zip (take n xsamples) ysamples)
