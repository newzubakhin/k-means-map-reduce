module Utils.Plot
    ( createGnuPlot
    )
where

import           Data.Map    (Map)
import qualified Data.Map    as Map
import           KMeans.Core (Cluster (..), Point (..))
import           System.IO

-- | Create a gnuplot data file for the output of the k-means algorithm
--
-- To plot the data, use
--
-- > plot "<<filename>>" u 1:2:3 with points palette
createGnuPlot :: Map Cluster ([Point], Point) -> Handle -> IO ()
createGnuPlot clusters h =
    mapM_ printPoint . flatten . zip colors . Map.toList $ clusters
  where
    printPoint (x, y, color) =
        hPutStrLn h $ show x ++ " " ++ show y ++ " " ++ show color

    flatten
        :: [(Float, (Cluster, ([Point], Point)))] -> [(Double, Double, Float)]
    flatten =
        concatMap
            (\(color, (_, (points, _))) -> map (\(x, y) -> (x, y, color)) points
            )

    colors :: [Float]
    colors = [0, 1 / fromIntegral (Map.size clusters) .. 1]
