{-# LANGUAGE ScopedTypeVariables #-}
module MapReduce.Local
    ( mapReduce
    , reducePerKey
    , groupByKey
    )
where

import           Data.Map        (Map, empty, insertWith, mapWithKey, toList)
import           MapReduce.Basic

-- /Google’s MapReduce Programming Model---Revisited/ by Ralf Laemmel
-- (<http://userpages.uni-koblenz.de/~laemmel/MapReduce/>).
mapReduce
    :: forall k1 k2 v1 v2 v3
     . Ord k2
    => MapReduce k1 v1 k2 v2 v3
    -> Map k1 v1
    -> Map k2 v3
mapReduce mr =
    reducePerKey mr    -- 3. Apply REDUCE to each group
        . groupByKey   -- 2. Group intermediate data per key
        . mapPerKey mr -- 1. Apply MAP to each key/value pair

reducePerKey :: MapReduce k1 v1 k2 v2 v3 -> Map k2 [v2] -> Map k2 v3
reducePerKey mr = mapWithKey (mrReduce mr) -- Apply REDUCE per key

groupByKey :: Ord k2 => [(k2, v2)] -> Map k2 [v2]
groupByKey = foldl insert empty
    where insert dict (k2, v2) = insertWith (++) k2 [v2] dict

mapPerKey :: MapReduce k1 v1 k2 v2 v3 -> Map k1 v1 -> [(k2, v2)]
mapPerKey mr =
    concatMap (uncurry $ mrMap mr)           -- 2. Concatenate with map MAP per-key list
                                   . toList  -- 1. Turn dictonary into list
