{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell     #-}
-- https://well-typed.com/blog/74/
-- http://hackage.haskell.org/package/distributed-static-0.3.8/docs/Control-Distributed-Static.html
module MapReduce.Distributed
    ( distributedMapReduce
    , __remoteTable
    )
where

import           Control.Distributed.Process
import           Control.Distributed.Process.Closure
import           Control.Distributed.Process.Serializable (Serializable)
import           Control.Distributed.Static               (closureApply, staticApply, staticCompose)
import           Control.Monad                            (forM_, replicateM, replicateM_)
import           Data.Binary                              (Binary, encode)
import           Data.ByteString.Lazy                     (ByteString)
import           Data.Map                                 (Map)
import qualified Data.Map                                 as Map (fromList, size, toList, unions)
import           Data.Typeable                            (Typeable)
import           Text.Printf                              (printf)

import           MapReduce.Basic                          (MapReduce (..))

import           MapReduce.Local                          (groupByKey, reducePerKey)

matchDict :: forall a b . SerializableDict a -> (a -> Process b) -> Match b
matchDict SerializableDict = match

sendDict :: forall a . SerializableDict a -> ProcessId -> a -> Process ()
sendDict SerializableDict = send

sdictProcessIdPair :: SerializableDict (ProcessId, ProcessId)
sdictProcessIdPair = SerializableDict

mapperProcess
    :: forall k1 v1 k2 v2 v3
     . SerializableDict (k1, v1)
    -> SerializableDict [(k2, v2)]
    -> (ProcessId, ProcessId)
    -> MapReduce k1 v1 k2 v2 v3
    -> Process ()
mapperProcess dictIn dictOut (master, workQueue) mr = getSelfPid >>= go
      where
          go us = do
              send workQueue us
              receiveWait
                  [ matchDict dictIn $ \(key, value) -> do
                    say "Received work -- mapping"
                    sendDict dictOut master (mrMap mr key value)
                    go us
                  , match $ \() -> return ()
                  ]

reducerProcess
    :: forall k1 v1 k2 v2 v3
     . SerializableDict (Map k2 [v2])
    -> SerializableDict (Map k2 v3)
    -> (ProcessId, ProcessId)
    -> MapReduce k1 v1 k2 v2 v3
    -> Process ()
reducerProcess dictIn dictOut (master, workQueue) mr = getSelfPid >>= go
    where
        go us = do
            send workQueue us
            receiveWait
                [ matchDict dictIn $ \m -> do
                    say "Received work -- reducing"
                    sendDict dictOut master (reducePerKey mr m)
                    go us
                , match $ \() -> return ()
                ]

remotable ['mapperProcess, 'reducerProcess, 'sdictProcessIdPair]

reducerProcessClosure
    :: forall k1 v1 k2 v2 v3
     . (Typeable k1, Typeable v1, Typeable k2, Typeable v2, Typeable v3)
    => Static (SerializableDict (Map k2 [v2]))
    -> Static (SerializableDict (Map k2 v3))
    -> Closure (MapReduce k1 v1 k2 v2 v3)
    -> ProcessId
    -> ProcessId
    -> Closure (Process ())
reducerProcessClosure dictIn dictOut mr master workQueue =
    closure decoder (encode (master, workQueue)) `closureApply` mr
        where
            decoder :: Static (ByteString -> MapReduce k1 v1 k2 v2 v3 -> Process ())
            decoder = ($(mkStatic 'reducerProcess) `staticApply` dictIn `staticApply` dictOut)
                `staticCompose` staticDecode $(mkStatic 'sdictProcessIdPair)

mapperProcessClosure
    :: forall k1 v1 k2 v2 v3
     . (Typeable k1, Typeable v1, Typeable k2, Typeable v2, Typeable v3)
    => Static (SerializableDict (k1, v1))
    -> Static (SerializableDict [(k2, v2)])
    -> Closure (MapReduce k1 v1 k2 v2 v3)
    -> ProcessId
    -> ProcessId
    -> Closure (Process ())
mapperProcessClosure dictIn dictOut mr master workQueue =
    closure decoder (encode (master, workQueue)) `closureApply` mr
  where
    decoder :: Static (ByteString -> MapReduce k1 v1 k2 v2 v3 -> Process ())
    decoder =
        ($(mkStatic 'mapperProcess) `staticApply` dictIn `staticApply` dictOut)
            `staticCompose` staticDecode $(mkStatic 'sdictProcessIdPair)

distributedMapReduce
    :: forall k1 k2 v1 v2 v3 a
     . ( Binary k1, Typeable k1 -- instance (Binary a, Typeable a) => Serializable a
       , Binary v1, Typeable v1
       , Binary k2, Typeable k2
       , Binary v2, Typeable v2
       , Binary v3, Typeable v3
       , Ord k2
       )
    => Static (SerializableDict (k1, v1))      -- map input
    -> Static (SerializableDict [(k2, v2)])    -- map output
    -> Static (SerializableDict (Map k2 [v2])) -- reduce input
    -> Static (SerializableDict (Map k2 v3))   -- reduce output
    -> Closure (MapReduce k1 v1 k2 v2 v3)
    -> [NodeId]
    -> ((Map k1 v1 -> Process (Map k2 v3)) -> Process a)
    -> Process a
distributedMapReduce dictIn dictOut dictInReduce dictOutReduce mr mappers p = do
    mr'       <- unClosure mr
    master    <- getSelfPid

    -- | Create a new typed channel, spawn a process on the local node, passing it
    -- the receive port, and return the send port
    workQueue :: SendPort (Maybe (k1, v1)) <- spawnChannelLocal $ \queue -> do
        let go :: Process ()
            go = do
                mWork <- receiveChan queue
                case mWork of
                    Just (key, val) -> do
                      -- As long there is work, make it available to the mappers
                        them <- expect
                        send them (key, val)
                        go
                    Nothing -> do
                      -- Tell the mappers to terminate
                        replicateM_ (length mappers) $ do
                            them <- expect
                            send them ()

                       -- Tell the master that the slaves are terminated
                        send master ()
        go

    reducersQueue :: SendPort (Maybe (Map k2 [v2])) <- spawnChannelLocal $ \queue -> do
        let go :: Process ()
            go = do
                mWork <- receiveChan queue
                case mWork of
                    Just m -> do
                      -- As long there is work, make it available to the mappers
                        them <- expect
                        send them m
                        go
                    Nothing -> do
                      -- Tell the mappers to terminate
                        replicateM_ (length mappers) $ do
                            them <- expect
                            send them ()

                       -- Tell the master that the slaves are terminated
                        send master ()
        go

    -- Start the mappers
    let workQueuePid = sendPortProcessId (sendPortId workQueue)
    let reducersWorkQueuePid = sendPortProcessId (sendPortId reducersQueue)
    forM_ mappers $ \nid -> do
        say $ printf "Spawning mapper %s" $ show nid
        spawn nid (mapperProcessClosure dictIn dictOut mr master workQueuePid)

    -- Start the reducers
    forM_ mappers $ \nid -> do
        say $ printf "Spawning reducer %s" $ show nid
        spawn nid (reducerProcessClosure dictInReduce dictOutReduce mr master reducersWorkQueuePid)

    let iteration :: Map k1 v1 -> Process (Map k2 v3)
        iteration input = do
            -- Make work available to the mappers
            mapM_ (sendChan workQueue . Just) (Map.toList input)

            -- Wait for the partial results from mappers
            partialsMappers <- replicateM (Map.size input) expect

            -- Groupping by key on this node
            let groupsForReduce = groupByKey . concat $ partialsMappers


            -- Make work available to the reducers
            let singleton x = [x]
            mapM_ (sendChan reducersQueue . Just . Map.fromList . singleton) (Map.toList groupsForReduce)


            -- Wait for the partial results from reducers
            say "Merging received partial results"
            partialsReducers <- replicateM (Map.size groupsForReduce) expect

            return (Map.unions partialsReducers)

    result <- p iteration

    -- Terminate the wrappers
    sendChan workQueue Nothing
    sendChan reducersQueue Nothing
    expect :: Process ()

    return result
