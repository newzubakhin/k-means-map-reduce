module MapReduce.Basic
    ( MapReduce(..)
    )
where

import           Data.Typeable (Typeable)

data MapReduce k1 v1 k2 v2 v3 = MapReduce {
    mrMap    :: k1 -> v1 -> [(k2, v2)]
  , mrReduce :: k2 -> [v2] -> v3
  } deriving (Typeable)
