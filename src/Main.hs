module Main where

import           Control.Distributed.Process
import           Control.Distributed.Process.Backend.SimpleLocalnet
import           Control.Distributed.Process.Node                   (initRemoteTable)
import           Control.Monad
import           Data.Array                                         (Array, listArray)
import           Data.Semigroup                                     ((<>))
import           Options.Applicative
import           System.IO
import           System.Random

import qualified KMeans.Core                                        as KMeans
import qualified MapReduce.Distributed                              as DistributedMapReduce
import qualified Utils.Plot                                         as Plot
import qualified Utils.SamplesGenerator                             as Gen

data Command =
    CommandMaster { host             :: String
                  , port             :: String
                  , numberOfClusters :: Int
                  , iterations       :: Int
                  , inputPath        :: String
                  } |
    CommandSlave { host :: String
                 , port :: String
                 } |
    CommandGenerate { numberOfClustes :: Int
                    , minp            :: Int
                    , maxp            :: Int
                    , seed            :: Int
                    }
                    deriving (Show)


rtable :: RemoteTable
rtable =
    DistributedMapReduce.__remoteTable . KMeans.__remoteTable $ initRemoteTable

main :: IO ()
main = do
    cmd <- execParser parserInfo'
    case cmd of
      CommandMaster mHost mPort mCln mIter mInput -> do
            rawPoints <- fmap lines . readFile $ mInput
            let points = map (readPoint . break (' '==)) rawPoints

            backend <- initializeBackend mHost mPort rtable
            startMaster backend $ \slaves ->
                if null slaves
                   then do
                       say "No slaves found -- terminate"
                       terminate
                   else do
                        clust <- liftIO $ replicateM mCln (atRandIndex points)
                        result <- KMeans.distrKMeans (arrayFromList points) clust slaves mIter
                        liftIO $ withFile "plot.data" WriteMode $ Plot.createGnuPlot result
      CommandSlave sHost sPort -> do
            backend <- initializeBackend sHost sPort rtable
            startSlave backend
      CommandGenerate gCln gMinp gMaxp gSeed -> Gen.generate gCln gMinp gMaxp gSeed


defaultHost = "127.0.0.1"

parserInfo' :: ParserInfo Command
parserInfo' = info' parser' "Example of distibuted mapReduce k-means"
    where
        parser' :: Parser Command
        parser' = (subparser . foldMap command')
            [ ("master", "Start master process", masterP)
            , ("slave", "Start slave process", slaveP)
            , ("generate", "Generate input data", generateP)
            ]

        masterP = CommandMaster
            <$> hostOpt
            <*> portOpt
            <*> clusterOpt
            <*> iterationOpt
            <*> inputOpt

        slaveP = CommandSlave
            <$> hostOpt
            <*> portOpt

        generateP = CommandGenerate
            <$> clusterOpt
            <*> intOpt "minp" "Minimum number of points in a cluster"
            <*> intOpt "maxp" "Maximum number of points in a cluster"
            <*> intOpt "seed" "Random seed"


        hostOpt = strOption
            (mconcat
                [ help "Hostname of local node"
                , value defaultHost
                , showDefault
                , short 'h'
                , long "host"
                , metavar "HOST"
                ]
            )

        portOpt = strOption
            (mconcat
                [ help "Port of local node"
                , showDefault
                , short 'p'
                , long "port"
                , metavar "PORT"
                ]
            )

        clusterOpt :: Parser Int
        clusterOpt = option auto
            (mconcat
                [ help "Number of clusters"
                , short 'c'
                , long "clnumber"
                , metavar "CLUSTERS"
                ]
            )

        iterationOpt = option auto
            (mconcat
                [ help "Number of iterations"
                , showDefault
                , value 10
                , short 'i'
                , long "iterations"
                , metavar "ITER"
                ]

            )
        inputOpt = argument str
            (mconcat
                [ help "Path to file with points cordinats"
                , metavar "FILE"
                ]
            )

        intOpt :: String -> String -> Parser Int
        intOpt name h = option auto
            (mconcat
                [ help h
                , long name
                , metavar "INT"
                ]
            )

        info' :: Parser a -> String -> ParserInfo a
        info' p desc = info (helper <*> p) (fullDesc <> progDesc desc)

        command' (cmdName, desc, parser) = command cmdName (info' parser desc)

randomPoint :: IO KMeans.Point
randomPoint = (,) <$> randomIO <*> randomIO

arrayFromList :: [e] -> Array Int e
arrayFromList xs = listArray (0, length xs - 1) xs

readPoint :: (String, String) -> (Double, Double)
readPoint (x,y) = (read x, read y)

atRandIndex :: [a] -> IO a
atRandIndex l = do
    i <- randomRIO (0, length l - 1)
    return $ l !! i
