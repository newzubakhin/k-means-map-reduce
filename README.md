# map-reduce
map-reduce is a simple polymorphic distributed implementation of K-Means in term of MapReduce using Cloud Haskell.

![map-reduce-algo](https://well-typed.com/blog/aux/images/ch-series/mapreduce.jpg)
# Build
```shell
stack build

```
# Usage

## Slave
```
Usage: map-reduce slave [-h|--host HOST] (-p|--port PORT)
  Start slave process

Available options:
  -h,--help                Show this help text
  -h,--host HOST           Hostname of local node (default: "127.0.0.1")
  -p,--port PORT           Port of local node

```

## Master
```
Usage: map-reduce master [-h|--host HOST] (-p|--port PORT)
                         (-c|--clnumber CLUSTERS) [-i|--iterations ITER] FILE
  Start master process

Available options:
  -h,--help                Show this help text
  -h,--host HOST           Hostname of local node (default: "127.0.0.1")
  -p,--port PORT           Port of local node
  -c,--clnumber CLUSTERS   Number of clusters
  -i,--iterations ITER     Number of iterations (default: 10)
  FILE                     Path to file with points cordinats

```

## Input generation
```
Usage: map-reduce generate (-c|--clnumber CLUSTERS) --minp INT --maxp INT
                           --seed INT
  Generate input data

Available options:
  -h,--help                Show this help text
  -c,--clnumber CLUSTERS   Number of clusters
  --minp INT               Minimum number of points in a cluster
  --maxp INT               Maximum number of points in a cluster
  --seed INT               Random seed

```

# Links
* [Well-Typed (Communication Patterns in Cloud Haskell (Part 4))](https://well-typed.com/blog/74/)

* [parconc-examples](https://github.com/simonmar/parconc-examples)

* [Google's MapReduce Programming Model -- Revisited](https://userpages.uni-koblenz.de/~laemmel/MapReduce/)

# References
1. Epstein, J., Black, A. P., and Peyton-Jones, S. Towards Haskell in the cloud, 2011
2. S. Marlow. Parallel and Concurrent Programming in Haskell. O’Reilly Media,  2013
